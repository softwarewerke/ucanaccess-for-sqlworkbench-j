# uca4sqlwb - UCanAccess for SQLWorkbench/J

2020 (c) softwarewerke.com

Do you want to access Microsoft Access database from SQLWorkbench/J (sqlwb) ?

Personally I always struggle to set it up. Here a quick one.

## How to ?

You have installed sqlwb and the folder available at SQLWB_HOME.

Download uca4sqlwb.jar from the repository download area and put into into SQLWB_HOME/ext.

Start sqlwb and configure the connect profile with UcanaccessDriver (see images/clip1.png).

Configure the UcanaccessDriver with the unique uca4sqlwb.jar (see images/clip2.png).

## MS-Access Database with password and/or Encoding

If the MS-Access database is password protected add to your JDBC URL 

    ;jackcessOpener=com.softwarewerke.uca4sqlwb.CryptCodecOpener

For different encodings you have:

    com.softwarewerke.uca4sqlwb.CryptCodecOpener
    com.softwarewerke.uca4sqlwb.CryptCodecOpenerUTF8
    com.softwarewerke.uca4sqlwb.CryptCodecOpenerWindows1252

## Requirements

Java 8 or later.

## Known Issues

None yet.

## References

All copyrights reserved to their respective owners

SQLWorkbench/J
https://www.sql-workbench.eu

UCanAccess
http://ucanaccess.sourceforge.net

