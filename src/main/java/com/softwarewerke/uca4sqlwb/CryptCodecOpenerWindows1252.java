package com.softwarewerke.uca4sqlwb;

import com.healthmarketscience.jackcess.CryptCodecProvider;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import net.ucanaccess.jdbc.JackcessOpenerInterface;

/**
 *
 * @author user
 */
public class CryptCodecOpenerWindows1252 implements JackcessOpenerInterface {

    @Override
    public Database open(File file, String pass) throws IOException {
        DatabaseBuilder dbBuilder = new DatabaseBuilder(file);
        dbBuilder.setAutoSync(false);
        dbBuilder.setCodecProvider(new CryptCodecProvider(pass));
        dbBuilder.setReadOnly(true);
        dbBuilder.setCharset(Charset.forName("Windows-1252"));
        return dbBuilder.open();
    }
}
